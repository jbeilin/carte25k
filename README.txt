Carte25k

Jacques Beilin - ENSG-Geomatique - 2020-2025

This plugin is an easy way to produce a paper map with coordinates on the side from current project

To produce a map :
- Open your project
- Start the plugin
- Set the paper format, paper orientation, map scale ant map CRS. 
- You can get a map from the current canvas center or from a custom center by clicking on the cross and the on a point on the map. 

The map ectent will be calculated to fit paper size and map scale.

The result will be a new layout with a map with grid and and coordinates with the selected CRS. 
